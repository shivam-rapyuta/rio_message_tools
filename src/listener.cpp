#include "ros/ros.h"
#include "std_msgs/String.h"
#include <time.h>

using namespace std;

bool csv = false;
double counter = 0.0;
double prev_value = -1.0;
time_t start = time(0);

bool isInflection( double current,  double previous)
{
   if(current < previous)
        return true;
   else
	return false;
}

double timeDiff(){
   double seconds_since_start = difftime( time(0), start);
}


bool isBeyondOneCycle(double diff, double RESET_DURATION) {
    if(diff >= RESET_DURATION)
    {
	start = time(0);
	return true;
    }
    else 
    {
	return false;
    }
}


double SAMPLE_DURATION=5;
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  double curr;
  sscanf(msg->data.c_str(), "%lf", &curr);
  double time_diff = timeDiff(); 
  
  //if(isInflection(curr, prev) || 
  counter = counter + 1.0;
  if(isBeyondOneCycle(time_diff, SAMPLE_DURATION))
  {
	  if (csv)
	  {
		  printf("%lf\t%lf\t%lf\n", counter, time_diff, counter/time_diff);
		  fflush(stdout);
	  }
	  else
	  {
		  ROS_INFO("I heard : [%lf] messages in [%lf] seconds. Rate: [%lf]Hz", counter, time_diff, counter/time_diff);
	  }
	  counter = 0.0;
  }
  prev_value = curr;
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener", ros::init_options::AnonymousName);
  ros::NodeHandle n;
  string node_name = ros::this_node::getName();
  n.getParam("sample_duration", SAMPLE_DURATION);
  n.getParam(node_name + "/csv", csv);
  ROS_INFO("Node: %s, CSV: %d", node_name.c_str(), csv);
  ros::Subscriber sub = n.subscribe("topic", 1000, chatterCallback);
  ros::spin();
  return 0;
}
