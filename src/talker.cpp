#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker1", ros::init_options::AnonymousName);
  ros::NodeHandle n("~");
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("/topic", 0);
  std::string message = "Hello World";
  int hz = 1000;

  n.getParam("message", message);
  n.getParam("hz", hz);
  std::cout<< hz;
  std::cout<< message;
  ros::Rate loop_rate(hz);

  int count = 0;
  while (ros::ok())
  {
    /**
     * This is a message object. You stuff it with data, and then publish it.
     */
    std_msgs::String msg;
    std::stringstream ss;
    ss << message << count;
    msg.data = ss.str();

    chatter_pub.publish(msg);
    
    ros::spinOnce();
    loop_rate.sleep();
    ++count;
  }

  return 0;
}
