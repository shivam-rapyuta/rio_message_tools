# rio_message_tools

A ROS1 catkin package for simple rostopic publish/subscribe  aswell as showing the state of the current ROS topic graph as a meta node.


# Installation
Clone the repo into your {catking workspace}/src/

At your {catkin_workspace} root, run

`$ catkin_make`

`$ source {catkin_workspace}/devel/setup.bash`
# Usage
Publish to topic /chatter at 500Hz

`rosrun rio_message_tools talker topic:=chatter _hz:=500     `

Report Message rate for topic /chatter

`rosrun rio_message_tools listener topic:=chatter  `
 
Introspect and publish ros_topic_graph on topic /rio_topic_graph as a json

`rosrun rio_message_tools messagelist `

 